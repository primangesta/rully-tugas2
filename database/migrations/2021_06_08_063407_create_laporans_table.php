<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateLaporansTable.
 */
class CreateLaporansTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('laporans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pelapor_id')->unsigned();
            $table->string('province');
            $table->string('city');
            $table->string('hazard');
            $table->text('description');
            $table->timestamps();
            $table->softDeletes();

			$table->foreign('pelapor_id')->references('id')->on('pelapors')->onDelete('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('laporans');
	}
}
