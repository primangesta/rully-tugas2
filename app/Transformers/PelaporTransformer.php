<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Pelapor;

/**
 * Class PelaporTransformer.
 *
 * @package namespace App\Transformers;
 */
class PelaporTransformer extends TransformerAbstract
{
    /**
     * Transform the Pelapor entity.
     *
     * @param \App\Entities\Pelapor $model
     *
     * @return array
     */
    public function transform(Pelapor $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
