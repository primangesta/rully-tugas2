<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Laporan;

/**
 * Class LaporanTransformer.
 *
 * @package namespace App\Transformers;
 */
class LaporanTransformer extends TransformerAbstract
{
    /**
     * Transform the Laporan entity.
     *
     * @param \App\Entities\Laporan $model
     *
     * @return array
     */
    public function transform(Laporan $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
