<?php

namespace App\Presenters;

use App\Transformers\PelaporTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class PelaporPresenter.
 *
 * @package namespace App\Presenters;
 */
class PelaporPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PelaporTransformer();
    }
}
