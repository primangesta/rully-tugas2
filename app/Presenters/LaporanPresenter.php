<?php

namespace App\Presenters;

use App\Transformers\LaporanTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class LaporanPresenter.
 *
 * @package namespace App\Presenters;
 */
class LaporanPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new LaporanTransformer();
    }
}
