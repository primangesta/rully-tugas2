<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Laporan.
 *
 * @package namespace App\Entities;
 */
class Laporan extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;

    protected $presenter = LaporanPresenter::class;
    public $incrementing = true;

    protected $fillable = [
        'id',
        'pelapor_id',
        'province',
        'city',
        'hazard',
        'description'
    ];

    protected $table = 'laporans';

    public function pelapor()
    {
        return $this->belongsTo(Pelapor::class);
    }

}
