<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Pelapor.
 *
 * @package namespace App\Entities;
 */
class Pelapor extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;

    protected $presenter = PelaporPresenter::class;
    public $incrementing = true;

    protected $fillable = [
        'id',
        'name',
        'phone',
    ];

    protected $table = 'pelapors';

    public function laporan()
    {
        return $this->hasOne(Laporan::class);
    }

}
