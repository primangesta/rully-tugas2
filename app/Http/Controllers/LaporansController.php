<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\LaporanCreateRequest;
use App\Http\Requests\LaporanUpdateRequest;
use App\Repositories\LaporanRepository;
use App\Validators\LaporanValidator;
use Ixudra\Curl\Facades\Curl;

use App\Entities\Laporan;

/**
 * Class LaporansController.
 *
 * @package namespace App\Http\Controllers;
 */
class LaporansController extends Controller
{
    /**
     * @var LaporanRepository
     */
    protected $repository;

    /**
     * @var LaporanValidator
     */
    protected $validator;

    /**
     * LaporansController constructor.
     *
     * @param LaporanRepository $repository
     * @param LaporanValidator $validator
     */
    public function __construct(LaporanRepository $repository, LaporanValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));

        $data = Laporan::select('*');

        if($request->has('pelapor_id')){
            $data = $data->where('pelapor_id', $request->pelapor_id);
        }

        if($request->has('province')){
            $data = $data->where('province', 'ilike', '%' . $request->province . '%');
        }

        if($request->has('city')){
            $data = $data->where('city', 'ilike', '%' . $request->city . '%');
        }

        if($request->has('hazard')){
            $data = $data->where('hazard', 'ilike', '%' . $request->hazard . '%');
        }

        if($request->has('description')){
            $data = $data->where('description', 'ilike', '%' . $request->description . '%');
        }

        $total = $data->count();
    
        if($request->has('limit')){
            $data->take($request->get('limit'));
            
            if($request->has('offset')){
            	$data->skip($request->get('offset'));
            }
        }

        if($request->has('order_type')){
            if($request->get('order_type') == 'asc'){
                if($request->has('order_by')){
                    $data->orderBy($request->get('order_by'));
                }else{
                    $data->orderBy('created_at');
                }
            }else{
                if($request->has('order_by')){
                    $data->orderBy($request->get('order_by'), 'desc');
                }else{
                    $data->orderBy('created_at', 'desc');
                }
            }
        }else{
            $data->orderBy('created_at', 'desc');
        }

        $data = $data->get();

        $response = [
            'status'    => true, 
            'message'   => 'Success',
            'total_row' => $total,
            'data'      => $data,
        ];

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  LaporanCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(LaporanCreateRequest $request)
    {
        DB::beginTransaction();
        try {
            $credentials = $request->only('pelapor_id');
            $rules = [
                'pelapor_id'  => 'required|exists:pelapors,id',
            ];
            
            $validator = Validator::make($credentials, $rules);
            if($validator->fails()) {
                return response()->json(['status'=> false, 'error'=> $validator->messages()],403);
            }
            
            $data = $this->repository->create($request->all());
            
            $response = [
                'status'  => true,
                'message' => 'Laporan created.',
                'data'    => $data->toArray(),
            ];

            DB::commit();
            return response()->json($response, 200);
        } catch (Exception $e) {
            // For rollback data if one data is error
            DB::rollBack();

            return response()->json([
                'status'    => false, 
                'error'     => 'Something wrong!',
                'exception' => $e
            ], 500);
        } catch (\Illuminate\Database\QueryException $e) {
            // For rollback data if one data is error
            DB::rollBack();

            return response()->json([
                'status'    => false, 
                'error'     => 'Something wrong!',
                'exception' => $e
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->repository->with(['pelapor'])->find($id);

        $response = [
            'status'  => true,
            'message' => 'Success',
            'data'    => $data,
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  LaporanUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(LaporanUpdateRequest $request, $id)
    {
        DB::beginTransaction();
        try {

            $credentials = $request->only('pelapor_id');
            $rules = [
                'pelapor_id'  => 'required|exists:pelapors,id',
            ];

            $validator = Validator::make($credentials, $rules);
            if($validator->fails()) {
                return response()->json(['status'=> false, 'error'=> $validator->messages()],403);
            }

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $data = $this->repository->update($request->all(), $id);

            $response = [
                'status'  => true,
                'message' => 'Laporan updated.',
                'data'    => $data->toArray(),
            ];

            DB::commit();
            return response()->json($response, 200);
        } catch (Exception $e) {
            // For rollback data if one data is error
            DB::rollBack();

            return response()->json([
                'status'    => false, 
                'error'     => 'Something wrong!',
                'exception' => $e
            ], 500);
        } catch (\Illuminate\Database\QueryException $e) {
            // For rollback data if one data is error
            DB::rollBack();

            return response()->json([
                'status'    => false, 
                'error'     => 'Something wrong!',
                'exception' => $e
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $deleted = $this->repository->delete($id);

            if($deleted){
                $response = [
                    'status'  => true,
                    'message' => 'Laporan deleted.'
                ];
    
                DB::commit();
                return response()->json($response, 200);
            }
            
        } catch (Exception $e) {
            // For rollback data if one data is error
            DB::rollBack();

            return response()->json([
                'status'    => false, 
                'error'     => 'Something wrong!',
                'exception' => $e
            ], 500);
        } catch (\Illuminate\Database\QueryException $e) {
            // For rollback data if one data is error
            DB::rollBack();

            return response()->json([
                'status'    => false, 
                'error'     => 'Something wrong!',
                'exception' => $e
            ], 500);
        }
    }
}
