<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\PelaporCreateRequest;
use App\Http\Requests\PelaporUpdateRequest;
use App\Repositories\PelaporRepository;
use App\Validators\PelaporValidator;
use Ixudra\Curl\Facades\Curl;

use App\Entities\Pelapor;

/**
 * Class PelaporsController.
 *
 * @package namespace App\Http\Controllers;
 */
class PelaporsController extends Controller
{
    /**
     * @var PelaporRepository
     */
    protected $repository;

    /**
     * @var PelaporValidator
     */
    protected $validator;

    /**
     * PelaporsController constructor.
     *
     * @param PelaporRepository $repository
     * @param PelaporValidator $validator
     */
    public function __construct(PelaporRepository $repository, PelaporValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));

        $data = Pelapor::select('*');

        if($request->has('name')){
            $data = $data->where('name', 'like', '%' . $request->name . '%');
        }

        if($request->has('phone')){
            $data = $data->where('phone', 'like', '%' . $request->phone . '%');
        }

        $total = $data->count();
    
        if($request->has('limit')){
            $data->take($request->get('limit'));
            
            if($request->has('offset')){
            	$data->skip($request->get('offset'));
            }
        }

        if($request->has('order_type')){
            if($request->get('order_type') == 'asc'){
                if($request->has('order_by')){
                    $data->orderBy($request->get('order_by'));
                }else{
                    $data->orderBy('created_at');
                }
            }else{
                if($request->has('order_by')){
                    $data->orderBy($request->get('order_by'), 'desc');
                }else{
                    $data->orderBy('created_at', 'desc');
                }
            }
        }else{
            $data->orderBy('created_at', 'desc');
        }

        $data = $data->get();

        $response = [
            'status'    => true, 
            'message'   => 'Success',
            'total_row' => $total,
            'data'      => $data,
        ];

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PelaporCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(PelaporCreateRequest $request)
    {
        DB::beginTransaction();
        try {
            $credentials = $request->only('name','phone');
            $rules = [
                'name'  => 'required',
                'phone' => 'required|unique:pelapors|regex:/(08)[0-9]{9}/',
            ];
            
            $validator = Validator::make($credentials, $rules);
            if($validator->fails()) {
                return response()->json(['status'=> false, 'error'=> $validator->messages()],403);
            }
            
            $data = $this->repository->create($request->all());
            
            $response = [
                'status'  => true,
                'message' => 'Pelapor created.',
                'data'    => $data->toArray(),
            ];

            DB::commit();
            return response()->json($response, 200);
        } catch (Exception $e) {
            // For rollback data if one data is error
            DB::rollBack();

            return response()->json([
                'status'    => false, 
                'error'     => 'Something wrong!',
                'exception' => $e
            ], 500);
        } catch (\Illuminate\Database\QueryException $e) {
            // For rollback data if one data is error
            DB::rollBack();

            return response()->json([
                'status'    => false, 
                'error'     => 'Something wrong!',
                'exception' => $e
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->repository->find($id);
        
        $response = [
            'status'  => true,
            'message' => 'Success',
            'data'    => $data,
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PelaporUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(PelaporUpdateRequest $request, $id)
    {
        DB::beginTransaction();
        try {

            $pelapor = Pelapor::find($id);
            if($pelapor->phone != $request->phone){
                $checkPhone = Pelapor::where('phone',$request->phone)->first();
                if($checkPhone){
                    $response = [
                        'status'  => false,
                        'message' => 'Phone number already exist.',
                    ];
        
                    DB::rollback();
                    return response()->json($response, 500);
                }
            }

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $data = $this->repository->update($request->all(), $id);

            $response = [
                'status'  => true,
                'message' => 'Pelapor updated.',
                'data'    => $data->toArray(),
            ];

            DB::commit();
            return response()->json($response, 200);
        } catch (Exception $e) {
            // For rollback data if one data is error
            DB::rollBack();

            return response()->json([
                'status'    => false, 
                'error'     => 'Something wrong!',
                'exception' => $e
            ], 500);
        } catch (\Illuminate\Database\QueryException $e) {
            // For rollback data if one data is error
            DB::rollBack();

            return response()->json([
                'status'    => false, 
                'error'     => 'Something wrong!',
                'exception' => $e
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $deleted = $this->repository->delete($id);

            if($deleted){
                $response = [
                    'status'  => true,
                    'message' => 'Pelapor deleted.'
                ];
    
                DB::commit();
                return response()->json($response, 200);
            }
            
        } catch (Exception $e) {
            // For rollback data if one data is error
            DB::rollBack();

            return response()->json([
                'status'    => false, 
                'error'     => 'Something wrong!',
                'exception' => $e
            ], 500);
        } catch (\Illuminate\Database\QueryException $e) {
            // For rollback data if one data is error
            DB::rollBack();

            return response()->json([
                'status'    => false, 
                'error'     => 'Something wrong!',
                'exception' => $e
            ], 500);
        }
    }
}
