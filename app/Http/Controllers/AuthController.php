<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator, Hash;
use DB;
use Mail;
use DateTime;

use App\Entities\User;
use App\Entities\Business;
use App\Entities\MBrand;
use App\Entities\MLocation;
use App\Entities\UserLocation;

use JWTAuth;
use JWTFactory;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{
    /**
     * API Register
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        DB::beginTransaction();
        try {
            $credentials = $request->only('email', 'password','password_confirmation');
        
            $rules = [
                'email' => 'required|unique:users',
                'password' => 'required|confirmed'
            ];
            
            $validator = Validator::make($credentials, $rules);
            if($validator->fails()) {
                return response()->json(['status'=> false, 'error'=> $validator->messages()],403);
            }

            $user = new User;                 
            $user->email = $request->email;                 
            $user->password = bcrypt($request->password);
            $user->save();
            
            DB::commit();
            $token = JWTAuth::fromUser($user);
            return response()->json(compact('user','token'),201);
        } catch (Exception $e) {
            // For rollback data if one data is error
            DB::rollBack();

            return response()->json([
                'status'=> false, 
                'error'=> 'Something wrong!',
                'exception' => $e
            ], 500);
        } catch (\Illuminate\Database\QueryException $e) {
            // For rollback data if one data is error
            DB::rollBack();

            return response()->json([
                'status'=> false, 
                'error'=> 'Something wrong!',
                'exception' => $e
            ], 500);
        }
    }

    /**
     * Get a JWT token via given credentials.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        DB::beginTransaction();
        try {
            $credentials = $request->only('email', 'password');

            try {
                if (! $token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials'], 400);
                }
            } catch (JWTException $e) {
                return response()->json(['error' => 'could_not_create_token'], 500);
            }

            return response()->json(compact('token'));
        } catch (Exception $e) {
            // For rollback data if one data is error
            DB::rollBack();

            $response = [
                'status'    => false,
                'error'     => 'Something error',
                'exception' => $e
            ];
    
            return response()->json($response, 500);
        } catch (\Illuminate\Database\QueryException $e) {
            // For rollback data if one data is error
            DB::rollBack();

            $response = [
                'status'    => false,
                'error'     => 'Something error',
                'exception' => $e
            ];
    
            return response()->json($response, 500);
        }
    }

    public function getAuthenticatedUser()
    {
        try {
            if(! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        }catch(Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        }catch(Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        }catch(Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }

        return response()->json(compact('user'));
    }

}