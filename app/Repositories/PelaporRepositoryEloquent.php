<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PelaporRepository;
use App\Entities\Pelapor;
use App\Validators\PelaporValidator;

/**
 * Class PelaporRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class PelaporRepositoryEloquent extends BaseRepository implements PelaporRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Pelapor::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return PelaporValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
