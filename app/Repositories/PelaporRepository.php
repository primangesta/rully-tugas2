<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PelaporRepository.
 *
 * @package namespace App\Repositories;
 */
interface PelaporRepository extends RepositoryInterface
{
    //
}
