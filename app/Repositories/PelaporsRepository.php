<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PelaporsRepository.
 *
 * @package namespace App\Repositories;
 */
interface PelaporsRepository extends RepositoryInterface
{
    //
}
