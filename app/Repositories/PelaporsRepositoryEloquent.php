<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PelaporsRepository;
use App\Entities\Pelapors;
use App\Validators\PelaporsValidator;

/**
 * Class PelaporsRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class PelaporsRepositoryEloquent extends BaseRepository implements PelaporsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Pelapors::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return PelaporsValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
