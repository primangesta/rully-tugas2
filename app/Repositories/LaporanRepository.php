<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LaporanRepository.
 *
 * @package namespace App\Repositories;
 */
interface LaporanRepository extends RepositoryInterface
{
    //
}
