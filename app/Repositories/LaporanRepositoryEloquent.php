<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\LaporanRepository;
use App\Entities\Laporan;
use App\Validators\LaporanValidator;

/**
 * Class LaporanRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class LaporanRepositoryEloquent extends BaseRepository implements LaporanRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Laporan::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return LaporanValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
