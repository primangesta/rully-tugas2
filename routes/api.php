<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('auth/register','AuthController@register');
Route::post('auth/login','AuthController@login');

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('pelapors','PelaporsController@index');
    Route::get('pelapors/{id}','PelaporsController@show');
    Route::post('pelapors','PelaporsController@store');
    Route::post('pelapors/{id}','PelaporsController@update');
    Route::delete('pelapors/{id}','PelaporsController@destroy');

    Route::get('laporans','LaporansController@index');
    Route::get('laporans/{id}','LaporansController@show');
    Route::post('laporans','LaporansController@store');
    Route::post('laporans/{id}','LaporansController@update');
    Route::delete('laporans/{id}','LaporansController@destroy');
});
